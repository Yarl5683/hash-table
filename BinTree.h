#ifndef UNTITLED7_BinTree_H
#define UNTITLED7_BinTree_H

#include "Checker.h"

#include <memory>

class BinTree : public Checker {
 public:
  BinTree();
  ~BinTree();

 private:
  struct Node {
    Node(const std::string& Word, const int& Hash)
        : Hash_data(Hash),
          Chain(" " + Word + " "),
          Left(nullptr),
          Right(nullptr) {}

    int Hash_data;
    std::string Chain;

    std::unique_ptr<Node> Left;
    std::unique_ptr<Node> Right;
  };

  void Add(const std::string&) override;

  bool Find(const std::unique_ptr<Node>&, const std::string&);
  bool Find(const std::string&) override;

  void Insert(std::unique_ptr<Node>&, const std::string&);
  std::unique_ptr<Node> Root;
};

#endif  // UNTITLED7_BinTree_H