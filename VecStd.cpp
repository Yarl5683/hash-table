#include "VecStd.h"
#include <algorithm>
#include "HashFunction.h"

VecStd::VecStd() : Checker("StdVect") {}

void VecStd::Add(const std::string& dict_word) {
  Bucket[Str2hash(dict_word) % 20].push_back(dict_word);
}

bool VecStd::Find(const std::string& search_word) {
  int Hash_word = Str2hash(search_word);

  auto begin = Bucket[Hash_word % 20].begin();
  auto end = Bucket[Hash_word % 20].end();

  return (std::find(begin, end, search_word) != end);
}

VecStd::~VecStd() = default;