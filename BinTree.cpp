#include "BinTree.h"
#include "HashFunction.h"

BinTree::BinTree() : Root(nullptr), Checker("BinTree") {}

void BinTree::Add(const std::string& New_word) { Insert(Root, New_word); }

void BinTree::Insert(std::unique_ptr<Node>& Leaf,
                     const std::string& Search_word) {
  int Hash_word = Str2hash(Search_word);

  if (Leaf == nullptr) {
    Leaf = std::make_unique<Node>(Search_word, Hash_word);
  } else {
    Hash_word > Leaf->Hash_data ? Insert(Leaf->Right, Search_word)
                                : Insert(Leaf->Left, Search_word);

    if (Hash_word == Leaf->Hash_data) {
      Leaf->Chain.append(" " + Search_word + " ");
    }
  }
}

bool BinTree::Find(const std::string& Search_word) {
  return Find(Root, Search_word);
}

bool BinTree::Find(const std::unique_ptr<Node>& Leaf,
                   const std::string& Search_word) {
  int Hash_word = Str2hash(Search_word);

  if (Leaf != nullptr) {
    if (Leaf->Hash_data == Hash_word) {
      return (Leaf->Chain.find(" " + Search_word + " ") != std::string::npos);
      //                return true;

      //          return false;
    }

    return Hash_word > Leaf->Hash_data ? Find(Leaf->Right, Search_word)
                                       : Find(Leaf->Left, Search_word);
  }

  return false;
}

BinTree::~BinTree() = default;
