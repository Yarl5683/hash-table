#ifndef UNTITLED7_HASHMAP_H
#define UNTITLED7_HASHMAP_H

#include <list>
#include <map>
#include "Checker.h"

class HashMap : public Checker {
 public:
  HashMap();
  ~HashMap();
  void WriteBadWord();

 private:
  void Add(const std::string&) override;
  bool Find(const std::string&) override;
  std::map<int, std::list<std::string>> Dict;
};

#endif  // UNTITLED7_HASHMAP_H