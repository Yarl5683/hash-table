#ifndef UNTITLED7_MYHASH_H
#define UNTITLED7_MYHASH_H

#include <vector>
#include "Checker.h"

class MyHash : public Checker {
 public:
  MyHash();
  ~MyHash();

 private:
  void Add(const std::string&) override;
  bool Find(const std::string&) override;

  std::string Bucket[52];
};

#endif  // UNTITLED7_MYHASH_H