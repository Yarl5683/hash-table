#ifndef UNTITLED7_VECSTD_H
#define UNTITLED7_VECSTD_H

#include "Checker.h"

#include <vector>

class VecStd : public Checker {
 public:
  VecStd();
  ~VecStd();

 private:
  void Add(const std::string&) override;
  bool Find(const std::string&) override;

  std::vector<std::string> Bucket[20];
};

#endif  // UNTITLED7_VECSTD_H
