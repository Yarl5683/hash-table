#include "HashMap.h"
#include "HashFunction.h"

#include <algorithm>
#include <fstream>

HashMap::HashMap() : Checker("HashTable") {}

void HashMap::Add(const std::string& dict_word) {
  Dict[Str2hash(dict_word)].push_back(dict_word);
}

void HashMap::WriteBadWord() {
  std::string filename = "bad_word_1.txt";

  for (int i = 0; i < Book_number; i++) {
    std::stringstream Data_stream(Books[i]);
    std::string books_item;

    std::ofstream BadWordFileS;

    BadWordFileS.open(filename);

    filename[9]++;  // change number of file

    while (Data_stream >> books_item) {
      auto list_start = Dict[Str2hash(books_item)].begin();
      auto list_end = Dict[Str2hash(books_item)].end();

      if (std::find(list_start, list_end, books_item) == list_end) {
        BadWordFileS << books_item << "\n";
      }
    }
    BadWordFileS.close();
  }
}

bool HashMap::Find(const std::string& search_word) {
  auto list_start = Dict[Str2hash(search_word)].begin();
  auto list_end = Dict[Str2hash(search_word)].end();

  return (std::find(list_start, list_end, search_word) != list_end);
}

HashMap::~HashMap() = default;
