#include "MyHash.h"

MyHash::MyHash() : Checker("MyHash") {}

void MyHash::Add(const std::string& Dict_word) {
  // Add new word to dictionary
  Bucket[Dict_word[0] * 2 - 2 * 'a'].append(" " + Dict_word + " ");
}

bool MyHash::Find(const std::string& Search_word) {
  return (Bucket[Search_word[0] * 2 - 2 * 'a'].find(" " + Search_word + " ") !=
          std::string::npos);
}

MyHash::~MyHash() = default;
