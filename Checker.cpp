#include "Checker.h"

#include <chrono>
#include <fstream>
#include <iostream>

std::string Checker::Dict;
std::string Checker::Books[Book_number];

Checker::Checker(std::string Struct_name) {
  // Read dictionary
  if (Dict.empty()) {
    ReadDictionary(Dict, "dictionary.txt");
  }

  Data.Hash_name = std::move(Struct_name);
  Data.Dict_load_time = 0;
  Data.All_word = 0;
  Data.Not_found_word = 0;

  // Read&format books for test (allowed only a-z, ' ', '`')
  std::string Books_name[Book_number]{"alice.txt", "dracula.txt", "holmes.txt",
                                      "sherlock.txt", "tolstoy.txt"};
  for (int i = 0; i < Book_number; i++) {
    if (Books[i].empty()) {
      ReadFile(Books[i], "./texts//" + Books_name[i]);
    }
  }
}

void Checker::LoadDict() {
  std::stringstream Dict_stream(Dict);
  std::chrono::time_point<std::chrono::system_clock> Start, End;

  std::string Dict_item;

  // start dictionary load
  Start = std::chrono::system_clock::now();

  while (Dict_stream >> Dict_item) {
    Add(Dict_item);
  }

  // end dictionary load
  End = std::chrono::system_clock::now();

  Data.Dict_load_time =
      std::chrono::duration_cast<std::chrono::milliseconds>(End - Start)
          .count();
}

void Checker::DataChecking() {
  std::chrono::time_point<std::chrono::system_clock> Start, End;

  // start texts checking
  Start = std::chrono::system_clock::now();

  for (int i = 0; i < Book_number; i++) {
    std::stringstream Data_stream(Books[i]);
    std::string Books_item;

    while (Data_stream >> Books_item) {
      Data.All_word++;

      if (!Find(Books_item)) {
        Data.Not_found_word++;
      }
    }
  }

  // end texts checking
  End = std::chrono::system_clock::now();

  Data.Text_processing_time =
      std::chrono::duration_cast<std::chrono::milliseconds>(End - Start)
          .count();
}

void Checker::PrintResult() {
  std::cout << Data.Hash_name << " " << Data.Dict_load_time << " "
            << Data.Text_processing_time << " " << Data.All_word << " "
            << Data.Not_found_word << "\n";
}

void Checker::ReadFile(std::string& Book, const std::string& Filename) {
  std::ifstream Read_text_stream(Filename);

  if (Read_text_stream.is_open()) {
    std::string Text_word;
    while (Read_text_stream >> Text_word) {
      for (int i = 0; i < Text_word.size(); i++) {
        // Parse data
        if (Text_word[i] >= 'a' && Text_word[i] <= 'z') {
          continue;
        }

        // From upper to lower case
        if (Text_word[i] >= 'A' && Text_word[i] <= 'Z') {
          Text_word[i] += 32;
          continue;
        }

        // delete unauthorized  characters
        if (Text_word[0] == '\'' || Text_word[Text_word.size() - 1] == '\'' ||
            !Allowed_symbol(Text_word[i])) {
          Text_word.erase(i, 1);
          i--;
        }
      }

      if (Text_word.size() > 1) {
        Book.append(Text_word + " ");
      }
    }
  }
  Read_text_stream.close();
}

void Checker::ReadDictionary(std::string& Dict_string,
                             const std::string& Filename) {
  std::ifstream Dict_read_stream(Filename);

  if (Dict_read_stream.is_open()) {
    std::string Dict_word;
    while (Dict_read_stream >> Dict_word) {
      Dict_string.append(" " + Dict_word + " ");
    }
  }

  Dict_read_stream.close();
}

bool Checker::Allowed_symbol(const char& S) {
  return (S == ' ' || S == '\'' || S == '\n');
}

Checker::~Checker() = default;
