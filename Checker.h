#ifndef UNTITLED7_CHECKER_H
#define UNTITLED7_CHECKER_H

#include <sstream>

class Checker {
 public:
  Checker(std::string = "Undefined");

  void LoadDict();
  void DataChecking();
  void PrintResult();

  ~Checker();

 private:
  void ReadFile(std::string&, const std::string&);
  void ReadDictionary(std::string&, const std::string&);

  bool Allowed_symbol(const char&);

  // All result out for each hash type
  struct Result {
    std::string Hash_name;
    double Dict_load_time;
    int Text_processing_time;
    int All_word;        // All word in all file
    int Not_found_word;  // All not found word in all file
  };

 protected:
  virtual void Add(const std::string&) = 0;
  virtual bool Find(const std::string&) = 0;

  static const int Book_number = 5;

  static std::string Dict;
  static std::string Books[Book_number];

  Result Data;
};

#endif  // UNTITLED7_CHECKER_H
